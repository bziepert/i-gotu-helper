﻿
add_gui_tooltip(wParam, lParam, Msg) {

	MouseGetPos,,,, control_classnn_under_mouse
	ControlGetText, control_text, %control_classnn_under_mouse%
	if(control_text<>"?") {
		ToolTip
		return
	}
	
	associated_variable := get_associated_variable_of_control_by_clasnn(control_classnn_under_mouse)
	
	if(associated_variable = "faq_download_data") {
		Help = Download tracks from trackers and save them in the`nfolder "files\track" as *.bin and *.track files. You can`nconvert this files on the tab "Manage files".
	} else if (associated_variable = "faq_erase_tracker_data") {
		Help = Erase the tracks that are saved on the trackers.`nTo prevent data loss it is not recommended to`nuse this function in combination with the`ndownload function. 
	} else if (associated_variable = "faq_track_to_gpx") {
		Help = Convert the downloaded *.track and *.bin files to`n*.gpx files and save them in folder "files\gpx".
	} else if (associated_variable = "faq_motion_detection") {
		Help = When enabled, the GPS Logger will switch on`nautomatically upon detection of motion. It is`nrecommended to disable this function for`nexperiments
	} else if (associated_variable = "faq_tracking_interval") {
		Help = Change the interval that a GPS coordinate is saved.
	} else if (associated_variable = "faq_use_power_saving") {
		Help = Whether to save the coordinates when`nthere is no movement. It is reccomended to`ndisable this function.
	} else if (associated_variable = "faq_change_tracker_settings") {
		Help = Whether you want the program to change`nthe hardware settings of the tracker.
	} else if (associated_variable = "faq_correct_timezone") {
		Help = You can add here add a number that will`nbe added to correct the timestamp.
	} else if (associated_variable = "faq_create_csv_file") {
		Help = Create a CSV file that uses ";"`nto seperate values.
	} else if (associated_variable = "faq_create_merged_csv_file") {
		Help = A ";" sperated CSV file that includes all`ntracks from the import folder. The file`nname will be added as column.
	} else {
		Help = No help text saved
	}
	
	
	
	
	ToolTip % Help

}

get_associated_variable_of_control_by_clasnn(clasnn) {
	
	global variable_array
	associated_variable := variable_array[clasnn]
	
	if(associated_variable = "") {
		
		GuiControlGet, classnn_of_keyboard_focus_before, Focus
		GuiControl Focus, %clasnn%
		GuiControlGet, associated_variable, FocusV
		GuiControl Focus, %classnn_of_keyboard_focus_before%
		
		if(associated_variable = "?") {
			RETURN
		}
		
		variable_array := Object(clasnn, associated_variable)
		
	}
	
	return associated_variable
	
}