add_convert_controls() {

	gui, tab, 2
	
	Gui, Font, bold
	Gui, Add, Text, section y+10 x22, Convert files
	Gui, Font, norm
	
	; ***** Enable correct timezone *****
	global enable_correct_timezone
	IniRead, enable_correct_timezone, settings\settings.ini, gui, enable_correct_timezone, 0
	static faq_enable_correct_timezone
	Gui, Add, Checkbox, y+7 x22 Checked%enable_correct_timezone% genable_correct_timezone venable_correct_timezone, Correct timezone
	Gui, Font, cblue underline
	Gui, Font, norm cblack
	
	if(enable_correct_timezone) {
		disableda := 0
	} else {
		disableda := 1
	}
	
	; ***** Correct Timezone *****
	global correct_timezone
	IniRead, correct_timezone, settings\settings.ini, gui, correct_timezone, 0
	static faq_correct_timezone
	
	Gui, Add, Edit, section x+3 vcorrect_timezone w40 disabled%disableda%, %correct_timezone%
	Gui, Add, Text, x+5, hours
	Gui, Font, cblue underline
	Gui, Add, Text, x+5 vfaq_correct_timezone, ?
	Gui, Font, norm cblack
	
	; ***** Create CSV files *****
	global create_csv_file
	IniRead, create_csv_file, settings\settings.ini, gui, create_csv_file, 0
	static faq_create_csv_file
	
	Gui, Add, Checkbox, y+12 x22 checked%create_csv_file% vcreate_csv_file, Create CSV file
	Gui, Font, cblue underline
	Gui, Add, Text, x+0 vfaq_create_csv_file, ?
	Gui, Font, norm cblack
	
	; ***** Create merged CSV files *****
	global create_merged_csv_file
	IniRead, create_merged_csv_file, settings\settings.ini, gui, create_merged_csv_file, 0
	static faq_create_merged_csv_file
	
	Gui, Add, Checkbox, y+7 x22 checked%create_merged_csv_file% vcreate_merged_csv_file, Create merged CSV file
	Gui, Font, cblue underline
	Gui, Add, Text, x+0 vfaq_create_merged_csv_file, ?
	Gui, Font, norm cblack
	
	; ***** Create GPX files *****
	global track_to_gpx
	IniRead, track_to_gpx, settings\settings.ini, gui, track_to_gpx, 0
	static faq_track_to_gpx
	
	Gui, Add, Checkbox, y+7 x22 checked%track_to_gpx% vtrack_to_gpx, Create GPX files
	Gui, Font, cblue underline
	Gui, Add, Text, x+0 vfaq_track_to_gpx, ?
	Gui, Font, norm cblack
	
}

enable_correct_timezone:
	restart_program()
RETURN