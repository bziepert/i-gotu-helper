return

create_gui() {

	global change_gui_tab

	IniRead, change_gui_tab, settings\settings.ini, general, selected_tab, 1
	Gui, Add, Tab2, w275 h235 altsubmit gchange_gui_tab vchange_gui_tab Choose%change_gui_tab%, Download and configure|Manage files

	add_download_controls()
	add_remove_tracks_controls()
	add_config_controls()
	add_convert_controls()
	add_view_files_controls()
	add_run_helper_controls()
	show_gui()
	OnMessage(0x200, "add_gui_tooltip")
	
}

change_gui_tab:

	gui, submit
	Sleep, 25
	gui, show
	IniWrite, %change_gui_tab%, settings\settings.ini, general, selected_tab

RETURN

show_gui() {

	; ***** get saved gui position *****
	IniRead, gui_position, settings\settings.ini, general, gui_position, Center
	
	; *****  show GUI *****
	Gui, Show, %gui_position%, i-gotU helper v0.02
	
}

add_run_helper_controls() {

	gui, tab

	Gui, Font, bold
	Gui, Add, Text,x10, Run
	Gui, Font, norm
	Gui, Add, Button, grun_helper x10, Run helper
	
}

add_view_files_controls() {
	
	Gui, Font, bold
	Gui, Add, Text, section y+10 x22, View files
	Gui, Font, norm
	
	Gui, Font, cblue underline
	Gui, Add, Text, section y+10 x22 gview_track, View downloaded files
	;Gui, Add, Text, x+5, ?
	Gui, Add, Text, section y+10 x22 gview_gpx, View gpx files
	;Gui, Add, Text, x+5, ?
	Gui, Add, Text, section y+10 x22 gview_csv, View csv files
	;Gui, Add, Text, x+5, ?
	Gui, Font, cblack norm
	
}



add_download_controls() {

	global use_download_function
	static faq_download_data
	IniRead, use_download_function, settings\settings.ini, download, download, 0
	
	Gui, Font, bold
	Gui, Add, Text,, Download tracks
	Gui, Font, norm
	
	Gui, Add, Checkbox, Checked%use_download_function% vuse_download_function, Download data from tracker
	Gui, Font, cblue underline
	Gui, Add, Text, x+0 vfaq_download_data, ?
	Gui, Font, norm cblack
	

}

add_remove_tracks_controls() {
	
	global erase_tracker_data
	static faq_erase_tracker_data
	IniRead, erase_tracker_data, settings\settings.ini, erase, erase_tracker_data, 0
	
	Gui, Font, bold
	Gui, Add, Text,y+10 x22, Remove tracks
	Gui, Font, norm
	
	Gui, Add, Checkbox, Checked%erase_tracker_data% verase_tracker_data, Remove tracks from tracker
	Gui, Font, cblue underline
	Gui, Add, Text, x+0 vfaq_erase_tracker_data, ?
	Gui, Font, norm cblack
	
}

GuiClose:
	close_program()
RETURN

view_track:
	run, files\track
RETURN

view_gpx:
	run, files\gpx
RETURN

view_csv:
	run, files\csv
RETURN