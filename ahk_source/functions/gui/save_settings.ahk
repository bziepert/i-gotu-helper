﻿write_settings_to_ini() {

	; ***** download *****
	global use_download_function
	IniWrite, %use_download_function%, settings\settings.ini, download, download
	
	; ***** erase *****
	global erase_tracker_data
	IniWrite, %erase_tracker_data%, settings\settings.ini, erase, erase_tracker_data
	
	; ***** config settings *****
	global change_tracker_settings
	IniWrite, %change_tracker_settings%, settings\settings.ini, gui, change_tracker_settings
	
	global use_motion_detection
	IniWrite, %use_motion_detection%, settings\settings.ini, gui, use_motion_detection
	
	global use_power_saving
	IniWrite, %use_power_saving%, settings\settings.ini, gui, use_power_saving
	
	global tracking_interval_minutes
	tracking_interval_minutes:=tracking_interval_minutes*1
	if(tracking_interval_minutes>60){
		tracking_interval_minutes:=60
	} else if (tracking_interval_minutes<0) {
		tracking_interval_minutes:=0
	}
	IniWrite, %tracking_interval_minutes%, settings\settings.ini, gui, tracking_interval_minutes
	
	global tracking_interval_seconds
	tracking_interval_seconds:=tracking_interval_seconds*1
	if(tracking_interval_seconds>59){
		tracking_interval_seconds:=59
	} else if (tracking_interval_seconds<0) {
		tracking_interval_seconds:=0
	}
	
	if(tracking_interval_minutes=0 and tracking_interval_seconds=0) {
		tracking_interval_seconds:=1
	} 
	
	IniWrite, %tracking_interval_seconds%, settings\settings.ini, gui, tracking_interval_seconds
	
	global block_user_input
	IniWrite, %block_user_input%, settings\settings.ini, config, block_user_input
	
	conversion_settings()
	
}

conversion_settings() {
	
	; ***** Enable correct timezone *****
	global enable_correct_timezone
	IniWrite, %enable_correct_timezone%, settings\settings.ini, gui, enable_correct_timezone
	
	; ***** Correct Timezone *****
	global correct_timezone
	correct_timezone := correct_timezone*1
	if (correct_timezone!="") {
		if(correct_timezone<-24) {
			correct_timezone := -24
		} else if (correct_timezone>24) {
			correct_timezone := 24
		}
	}
	IniWrite, %correct_timezone%, settings\settings.ini, gui, correct_timezone
	
	; ***** Create CSV files *****
	global create_csv_file
	IniWrite, %create_csv_file%, settings\settings.ini, gui, create_csv_file
	
	; ***** Create merged CSV files *****
	global create_merged_csv_file
	IniWrite, %create_merged_csv_file%, settings\settings.ini, gui, create_merged_csv_file
	
	; ***** Create GPX files *****
	global track_to_gpx
	IniWrite, %track_to_gpx%, settings\settings.ini, gui, track_to_gpx
	
}