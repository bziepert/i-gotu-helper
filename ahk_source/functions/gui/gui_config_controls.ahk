
add_config_controls() {

	Gui, Font, bold
	Gui, Add, Text, section y+10 x22, Configure tracker
	Gui, Font, norm
	
	global change_tracker_settings
	IniRead, change_tracker_settings, settings\settings.ini, gui, change_tracker_settings, 0
	static faq_change_tracker_settings
	Gui, Add, Checkbox, y+7 x22 Checked%change_tracker_settings% gchange_tracker_settings vchange_tracker_settings, Change tracker settings
	Gui, Font, cblue underline
	Gui, Add, Text, x+0 vfaq_change_tracker_settings, ?
	Gui, Font, norm cblack
	
	if(change_tracker_settings) {
		disabledb := 0
	} else {
		disabledb := 1
	}
	
	global use_motion_detection
	IniRead, use_motion_detection, settings\settings.ini, gui, use_motion_detection, 0
	static faq_motion_detection
	Gui, Add, Checkbox, y+7 x22 Disabled%disabledb% Checked%use_motion_detection% vuse_motion_detection, Use motion detection
	Gui, Font, cblue underline
	Gui, Add, Text, x+0 vfaq_motion_detection, ?
	Gui, Font, norm cblack
	
	global use_power_saving
	IniRead, use_power_saving, settings\settings.ini, gui, use_power_saving, 0
	static faq_use_power_saving
	Gui, Add, Checkbox, y+7 x22 Disabled%disabledb% Checked%use_power_saving% vuse_power_saving, Use power saving
	Gui, Font, cblue underline
	Gui, Add, Text, x+0 vfaq_use_power_saving, ?
	Gui, Font, norm cblack
	
	global tracking_interval_minutes
	IniRead, tracking_interval_minutes, settings\settings.ini, gui, tracking_interval_minutes, 5
	
	global tracking_interval_seconds
	IniRead, tracking_interval_seconds, settings\settings.ini, gui, tracking_interval_seconds, 0
	
	static faq_tracking_interval
	
	Gui, Add, Text, y+10 x22, Tracking interval
	
	Gui, Add, Edit, x+7 w25 Disabled%disabledb% vtracking_interval_minutes, %tracking_interval_minutes%
	Gui, Add, Text, x+7, min.
	Gui, Add, Edit, x+7 w25 Disabled%disabledb% vtracking_interval_seconds, %tracking_interval_seconds%
	Gui, Add, Text, x+7, sec.
	
	Gui, Font, cblue underline
	Gui, Add, Text, x+3 vfaq_tracking_interval, ?
	Gui, Font, norm cblack
	
}

change_tracker_settings:
	restart_program()
RETURN