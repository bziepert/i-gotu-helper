﻿RETURN

run_helper:

	; ***** close GUI and save settings *****
	gui, submit
	gui, destroy
	write_settings_to_ini()
	
	IniRead, change_gui_tab, settings\settings.ini, general, selected_tab, 1
	
	if (change_gui_tab <> "1") {
		run_file_conversion_steps()
		RETURN
	}
	
	; ***** Check if @trip PC is still running *****
	Loop {
		
		IfWinExist, @trip PC
		{
			MsgBox, 48, Please close @trip PC, Please close @trip PC or a file / folder with the same name.
		} else {
			Break
		}
		
	}
	
	; ***** Confirm close all work *****
	MsgBox, 49, Before you procede, This programs takes over the control of your computer. Before you procede:`n- Save your work`n- Close all programs`n- Disconnect any GPS trackers`n`nThis program blocks you mouse and keyboard input. You can stop the program with the key combination [Ctrl]+[Esc] and regain control.
	IfMsgBox OK
		process_helper_steps()
	else IfMsgBox Cancel
		create_gui()
	RETURN

RETURN

process_helper_steps() {
	
	; ***** process the different steps of the helper *****
	
	; ***** Confirm @trip PC whipe *****
	MsgBox, 49, Data in @trip PC will be deleted, Please be aware that all saved tracks in @trip PC will be deleted. Only procede if you have exportet all tracks to another program.
	IfMsgBox OK
	{	
		
		Gui, Add, Text,, While the program is working you will see instructions in the Tray Tip. Please follow these instructions.`n`nExample:
		Gui, Add, Picture,, includes\example_tray_tip.png
		Gui, Add, Button, Default gclose_message_for_tray, I will follow the instruction in the Tray Tip
		Gui, Show,, Please follow instructions in Tray Tip
		
	} else {
		create_gui()
	}
	
}

close_message_for_tray:
	
	gui, destroy
	run_helper_with_a_trip_connection()
	
RETURN

scanner_connection_needed() {
	
	IniRead, use_download_function, settings\settings.ini, download, use_download_function
	IniRead, use_config_function, settings\settings.ini, config, use_config_function
	
	if(use_download_function=1 OR use_config_function=1 ){
		return 1
	} else {
		return 0
	}
	
}

run_helper_with_a_trip_connection() {
	
	run_a_trip_pc()
	
	BlockInput, On
	IniWrite, 1, settings\settings.ini, macro, blockinput
	global block_input_active
	block_input_active := 1
	
	delete_all_atrip_pc_files()
	TrayTip, Starting, Starting @trip PC , 10
	Sleep, 10000
	WaitDisconnected()
	
	Loop {

		wait_connected()
		download()
		delete_tracks()
		open_hardware_settings()
		WaitDisconnected()
	
	}
	
}