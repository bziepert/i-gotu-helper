﻿
convert_track_to_gpx(source,target){

	TrayTip, Processing, Begin, 5

	Loop, %source%\*.track {
		StringTrimRight, track_name, A_LoopFileName, 6
		track_data := get_gpx_data(track_name, source)
		
		filename = %target%\%track_name%.gpx
		
		IfExist, %filename%
		{
			FileDelete, %filename% 
		}
		
		FileAppend, %track_data%, %filename%
	}
	
	TrayTip, Processing, End, 5
	
}

get_gpx_data(track_name, source) {

	TrayTip, Processing, Convert file %track_name%, 10

	t=

	IniRead, PointCount, %source%\%track_name%.track, Track, PointCount
	IniRead, DeviceId, %source%\%track_name%.track, Track, DeviceId
	IniRead, StartTime, %source%\%track_name%.track, Track, StartTime
	IniRead, EndTime, %source%\%track_name%.track, Track, EndTime
	
	t = %t%<?xml version="1.0" encoding="UTF-8" standalone="yes" ?>`n
	t = %t%<gpx creator="http://analyse-gps.com" version="1.0" xmlns="http://www.topografix.com/GPX/1/0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.topografix.com/GPX/1/0 http://www.topografix.com/GPX/1/0/gpx.xsd">`n
	
	IniRead, POI_0, %source%\%track_name%.bin, POI_%DeviceId%, POI_0
	
	if(POI_0 <> "ERROR") {
	
		StringSplit, point_array, POI_0, `,
		gpx_time := unix2gpx(point_array1)
	
		t = %t%<wpt lat="%point_array2%" lon="%point_array3%">`n
		t = %t%<ele>%point_array4%</ele>`n
		t = %t%<time>%gpx_time%</time>`n
		
	} else {
		
		wpt := get_new_track_point(source, track_name, DeviceId, 0, 1)
		t = %t%%wpt%
		
	}
	
	t = %t%<name></name>`n
	t = %t%<desc></desc>`n
	t = %t%<sym>Waypoint</sym>`n
	t = %t%</wpt>`n
	
	t = %t%<trk>`n
	t = %t%<name>%track_name%</name>`n
	t = %t%<desc>Color:004000ff</desc>`n
	t = %t%<trkseg>`n
	
	Loop, %PointCount% {
		i := a_index-1
		new_track_point := get_new_track_point(source, track_name, DeviceId, i, 0)
		t = %t%%new_track_point%
	}
	
	t = %t%</trkseg>`n
	t = %t%</trk>`n
	t = %t%</gpx>
	
	return t
}

unix2gpx(unixTimestamp) {
	returnDate = 19700101000000
	returnDate += unixTimestamp, s
	
	IniRead, enable_correct_timezone, settings\settings.ini, gui, enable_correct_timezone, 0
	
	if(enable_correct_timezone=1) {
		IniRead, correct_timezone, settings\settings.ini, gui, correct_timezone, 0
		EnvAdd, returnDate, %correct_timezone%, Hours
	}
	
	FormatTime, returnDate, %returnDate%, yyyy-MM-ddTHH:mm:ssZ
	return returnDate
}

AddHours(hours,time)
{
	StringReplace, time, time, T,, All
	StringReplace, time, time, Z,, All
	StringReplace, time, time, -,, All
	StringReplace, time, time, :,, All
	
	EnvAdd, time, %hours%, Hours
	
	FormatTime, ChangedTime, %time%, yyyy-MM-ddTHH:mm:ssZ
	
	return ChangedTime
}

get_new_track_point(source, track_name, DeviceId, i, wpt) {
	
	Point=
	new_track_point=
	
	IniRead, Point, %source%\%track_name%.bin, Point_%DeviceId%, Point_%i%
	
	StringSplit, point_array, Point, `,
	gpx_time := unix2gpx(point_array1)
	speed := round(point_array5/3600,2)
	
	if(wpt) {
		new_track_point = %new_track_point%<wpt lat="%point_array2%" lon="%point_array3%">`n
		new_track_point = %new_track_point%<ele>%point_array4%</ele>`n
		new_track_point = %new_track_point%<time>%gpx_time%</time>`n
	} else {
		new_track_point = %new_track_point%<trkpt lat="%point_array2%" lon="%point_array3%">`n
		new_track_point = %new_track_point%<ele>%point_array4%</ele>`n
		new_track_point = %new_track_point%<time>%gpx_time%</time>`n
		new_track_point = %new_track_point%<speed>%speed%</speed>`n
		new_track_point = %new_track_point%</trkpt>`n
	}
	
	RETURN new_track_point
	
}
