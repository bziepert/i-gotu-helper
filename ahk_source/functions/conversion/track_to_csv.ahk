﻿
convert_track_to_csv(source,target){

	TrayTip, Processing, Begin, 5

	Loop, %source%\*.track {
		
		StringTrimRight, track_name, A_LoopFileName, 6
		track_data := get_csv_data(track_name, source)
		
		filename = %target%\%track_name%.csv
		IfExist, %filename%
		{
			FileDelete, %filename% 
		}
		
		FileAppend, %track_data%, %filename%
		
	}
	
	TrayTip, Processing, End, 5
	
}

get_csv_data(track_name, source) {

	TrayTip, Processing, Convert file %track_name%, 10

	IniRead, PointCount, %source%\%track_name%.track, Track, PointCount
	IniRead, DeviceId, %source%\%track_name%.track, Track, DeviceId
	IniRead, StartTime, %source%\%track_name%.track, Track, StartTime
	IniRead, EndTime, %source%\%track_name%.track, Track, EndTime	
	IniRead, POI_0, %source%\%track_name%.bin, POI_%DeviceId%, POI_0
	
	t="File";"Latitude";"Longitude";"Altitude";"Date and Time";"Speed"`n	
	
	Loop, %PointCount% {
		i := a_index-1
		new_track_point := get_new_track_row(source, track_name, DeviceId, i, 0)
		t = %t%%new_track_point%
	}
	
	return t
}

unix2csv(unixTimestamp) {
	returnDate = 19700101000000
	returnDate += unixTimestamp, s
	
	IniRead, enable_correct_timezone, settings\settings.ini, gui, enable_correct_timezone, 0
	
	if(enable_correct_timezone=1) {
		IniRead, correct_timezone, settings\settings.ini, gui, correct_timezone, 0
		EnvAdd, returnDate, %correct_timezone%, Hours
	}
	
	FormatTime, returnDate, %returnDate%, yyyy-MM-dd HH:mm:ss
	return returnDate
}

get_new_track_row(source, track_name, DeviceId, i, wpt) {
	
	Point=
	new_track_point=
	
	IniRead, Point, %source%\%track_name%.bin, Point_%DeviceId%, Point_%i%
	
	StringSplit, point_array, Point, `,
	gpx_time := unix2csv(point_array1)
	speed := round(point_array5/3600,2)

	new_track_point = %new_track_point%"%track_name%";
	new_track_point = %new_track_point%"%point_array2%";
	new_track_point = %new_track_point%"%point_array3%";
	new_track_point = %new_track_point%"%point_array4%";
	new_track_point = %new_track_point%"%gpx_time%";
	new_track_point = %new_track_point%"%speed%"`n
	
	RETURN new_track_point
	
}
