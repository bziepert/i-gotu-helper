﻿run_file_conversion_steps() {

	IniRead, track_to_gpx, settings\settings.ini, gui, track_to_gpx, 0
	if(track_to_gpx) {
		convert_track_to_gpx("files\track","files\gpx")
	}

	IniRead, create_csv_file, settings\settings.ini, gui, create_csv_file, 0
	if(create_csv_file) {
		convert_track_to_csv("files\track","files\csv")
		
		IniRead, create_merged_csv_file, settings\settings.ini, gui, create_merged_csv_file, 0
		if(create_merged_csv_file) {
			create_merged_csv("files\csv")
		}
		
	}
	
	restart_program()
	
}