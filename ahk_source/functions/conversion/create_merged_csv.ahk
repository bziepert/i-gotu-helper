
create_merged_csv(target) {

	t=

	Loop, %target%\*.csv {
		
		file_content=
		FileRead, file_content, %target%\%A_LoopFileName%
		
		t=%t%%file_content%`n
		
	}
	
	filename = %target%\merged.csv
	IfExist, %filename%
	{
		FileDelete, %filename% 
	}
	
	FileAppend, %t%, %filename%

}

