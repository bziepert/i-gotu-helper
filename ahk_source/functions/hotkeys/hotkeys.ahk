
; ***** pause script *****
LControl & p::

	global block_input_active
	if(block_input_active) {
		blockinput, Off
	}
	
	Suspend
	Pause
	Suspend 
	
	if(block_input_active) {
		blockinput, On
	}
	
RETURN

; ***** restart program *****
LControl & r::

	restart_program()

RETURN

; ***** close program *****
LControl & ESC::

	close_program()

RETURN