change_power_saving() {

	IniRead, use_power_saving, settings\settings.ini, gui, use_power_saving, 0
	
	MouseMove, 389, 224
	MouseGetPos, MouseX, MouseY
	PixelGetColor, color, %MouseX%, %MouseY%

	if color = 0x000000
	{
		enabled := 1
	} else {
		enabled := 0
	}
	
	if(use_power_saving != %enabled%) {
		click 389, 224
	}
	
	sleep, 5000
	
}