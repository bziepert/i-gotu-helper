change_motion_detection() {

	IniRead, use_motion_detection, settings\settings.ini, gui, use_motion_detection, 0
	
	MouseMove, 164, 415
	MouseGetPos, MouseX, MouseY
	PixelGetColor, color, %MouseX%, %MouseY%

	if color = 0x000000
	{
		enabled := 1
	} else {
		enabled := 0
	}
	
	if(use_motion_detection != %enabled%) {
		click 164, 415
	}
	
	sleep, 5000
	
}