open_hardware_settings() {

	IniRead, change_tracker_settings, settings\settings.ini, gui, change_tracker_settings, 0
	if (change_tracker_settings=0) {
		RETURN
	}
	
	open_hardware_settings_window()
	change_tracking_interval()
	change_power_saving()
	change_motion_detection()
	Sleep, 500
	save_hardware_settings_window()
	
}

open_hardware_settings_window() {

	WinActivate, @trip PC
	Click 514 54
	Sleep, 500

	WinWait,, All GPS logs and configurations on the device will be cleared.
	WinActivate,, All GPS logs and configurations on the device will be cleared.
	WinWaitActive,, All GPS logs and configurations on the device will be cleared.
	
	Send, !n
	
	WinWait,, Choose Startup Method
	WinActivate,, Choose Startup Method
	WinWaitActive,, Choose Startup Method
	
	Send, !n
	
	WinWait,, Waypoint logging interval
	WinActivate,, Waypoint logging interval
	WinWaitActive,, Waypoint logging interval
	
}

save_hardware_settings_window() {

	WinWait,, Waypoint logging interval
	WinActivate,, Waypoint logging interval
	WinWaitActive,, Waypoint logging interval
	
	Send, !n
	
	WinWait,, Device configurations are now complete.
	WinActivate,, Device configurations are now complete.
	WinWaitActive,, Device configurations are now complete.
	
	Send, {ENTER}

}