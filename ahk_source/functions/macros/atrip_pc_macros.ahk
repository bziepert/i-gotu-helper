﻿RETURN

run_a_trip_pc() {
	IniRead, a_trip_pc_dir, settings\settings.ini, atrippc, dir
	Run, *RunAs %a_trip_pc_dir%
}

wait_connected() {

	close_unwanted_windows()

	loop {

		TrayTip, Connect the first/next tracker & wait, Please connect the tracker and wait! This can take up to 30 seconds.`n`nStop with [Ctrl]+[Esc], 5

		WinWait, @trip PC
		WinActivate, @trip PC
		
		close_unwanted_windows()
		
		IfWinExist,, Thanks for using @trip PC Import Wizard.
		{
			WinActivate,, Thanks for using @trip PC Import Wizard.
			Send, !n
		}
		
		WinActivate, @trip PC
		MouseMove, 511, 54
		MouseGetPos, MouseX, MouseY
		PixelGetColor, color, %MouseX%, %MouseY%

		if color = 0xC4BFBF
		{
			break
		}

		Sleep, 1000

	}
	
}

delete_tracks() {
	
	global global erase_tracker_data
	if(global erase_tracker_data == 0) {
		RETURN
	}
	
	TrayTip, Erase data, Erase data from tracker`n`nStop with [Ctrl]+[Esc] , 5

	close_unwanted_windows()
	
	WinActivate, @trip PC
	click 545 54
	
	WinWait,,Do you want to clear existing data in
	WinActivate,,Do you want to clear existing data in
	Send, {ENTER}
	
	WinWait,,Log data cleared successfully!
	WinActivate,,Log data cleared successfully!
	Send, {ENTER}
	
}

IsTrackInDir(Dir){
	Loop %Dir%\*.track, 0, 1
	{
		return 0
	}
	return 1
}

store_tracker_name(is_empty) {
	
	close_unwanted_windows()
	destination_file = %A_ScriptDir%\files\track\ids.csv
	tracker_id := ask_user_for_tracker_id()
	
	if(is_empty) {
		FileAppend, %tracker_id%; no tracks on the tracker`n, %destination_file%
		return tracker_id
	}
	
	source_bin = %A_AppData%\Mobile Action\atrip\Track\*.bin
	source_track = %A_AppData%\Mobile Action\atrip\Track\*.track
	
	Loop, %source_track% {
		FileAppend, %tracker_id%_%A_LoopFileName%;%tracker_id%`n, %destination_file%
	}
	
	return tracker_id
	
}

ask_user_for_tracker_id() {

	blockinput, Off
	IniWrite, 0, settings\settings.ini, macro, blockinput
	global block_input_active
	block_input_active := 0
	
	Loop {
	
		InputBox, tracker_id, tracker ID, What is the tracker id?
		if ErrorLevel
		{
			restart_program()
		} else {
			if (tracker_id <> "") {
				blockinput, On
				IniWrite, 1, settings\settings.ini, macro, blockinput
				global block_input_active
				block_input_active := 1
				return tracker_id
			}
		}
		
	}
	
}

WaitDisconnected() {
	
	close_unwanted_windows()
	
	Loop
	{

		TrayTip, Please disconnect tracker, Please disconnect the tracker`n`nIf this is the last tracker you can finish with [Ctrl]+[Esc] , 5
	
		close_unwanted_windows()
		
		WinActivate, @trip PC
		WinWaitActive, @trip PC
		
		MouseMove, 511, 54
		MouseGetPos, MouseX, MouseY
		PixelGetColor, color, %MouseX%, %MouseY%

		if color = 0xD6D6D6
		{
			break
		}
		
		Sleep, 1000

	}

}

click_next_on_welcome_import_wizard() {
	
	IfWinExist,, Thanks for using @trip PC Import Wizard
	{
		WinActivate,, Thanks for using @trip PC Import Wizard.
		WinWaitActive,, Thanks for using @trip PC Import Wizard.
		Send, !n
	}
	
	IfWinExist,, No data is found in device.
	{
		WinActivate,, No data is found in device.
		WinWaitActive,, No data is found in device.
		Send, !n
		
		WinWait,, Please connect GPS Device to PC to
		WinActivate,, Please connect GPS Device to PC to
		WinWaitActive,, Please connect GPS Device to PC to
		Send, {ESC}
	}
	
}

delete_all_atrip_pc_files(){
	
	TrayTip, Remove old tracks, Remove stored tracks in @trip PC`n`nStop with [Ctrl]+[Esc], 5
	
	Loop, %A_AppData%\Mobile Action\atrip\Track\*.*, 1, 1
	{
		FileDelete, %A_LoopFileFullPath% ; Delete file
	}
	
}