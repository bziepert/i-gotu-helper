change_tracking_interval() {
	
	IniRead, tracking_interval_minutes, settings\settings.ini, gui, tracking_interval_minutes, 5
	IniRead, tracking_interval_seconds, settings\settings.ini, gui, tracking_interval_seconds, 0
	
	MouseMove, 200, 225
	Click 2
	Send, %tracking_interval_minutes%
	
	MouseMove, 300, 225
	Click 2
	Send, %tracking_interval_seconds%
	
}