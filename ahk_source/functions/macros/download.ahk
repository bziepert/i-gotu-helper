download() {

	global use_download_function
	if (use_download_function = 0) {
		RETURN
	}
	
	start_download()
	wait_for_download()
	fetch_files()
	
}

start_download() {

	close_unwanted_windows()

	TrayTip, Download, Starting download`n`nStop with [Ctrl]+[Esc] , 5

	Sleep, 500
	
	close_unwanted_windows()
	
	WinActivate, @trip PC
	Click 38 51
	Sleep, 500
	
	GroupAdd, wait_after_download,, Thanks for using @trip PC Import Wizard
	GroupAdd, wait_after_download,, No data is found in device.
	WinWait ahk_group wait_after_download
	
	;WinWait,, Thanks for using @trip PC Import Wizard
	click_next_on_welcome_import_wizard()
	Sleep, 500
	
}

wait_for_download() {

	close_unwanted_windows()

	TrayTip, Download, Waiting for download`n`nStop with [Ctrl]+[Esc] , 5

	Loop
	{

		IfWinExist,, No new data is found in device.
		{
			WinActivate,, No new data is found in device.
			Send, {ENTER}
			Sleep, 500
			
			WinWait,, Download Data
			WinActivate,, Download Data
			Send, {ESC}
			Sleep, 500
			
			break
		}
	
		IfWinExist,, No data is found in device.
		{
			WinActivate,, No data is found in device.
			WinWaitActive,, No data is found in device.
			Send, {ENTER}
			Sleep, 500
			
			WinWait,, Download Data
			WinActivate,, Download Data
			Send, {ESC}
			Sleep, 500
			
			break
		}
	
		IfWinExist,, Create Trip
		{
			WinActivate,, Create Trip
			Send, {ESC}
			Sleep, 500
			break
		}

		Sleep, 1000

	}
}

fetch_files() {
	
	close_unwanted_windows()
	source = %A_AppData%\Mobile Action\atrip\Track
	is_empty := IsTrackInDir(source)
	
	if(is_empty) {
		tracker_id := store_tracker_name(1)
		RETURN
	}
	
	tracker_id := store_tracker_name(0)
	
	TrayTip, Download, Fetching files`n`nStop with [Ctrl]+[Esc] , 5

	source_bin = %A_AppData%\Mobile Action\atrip\Track\*.bin
	source_track = %A_AppData%\Mobile Action\atrip\Track\*.track
	destination = %A_ScriptDir%\files\track

	FileMove, %source_bin%, %destination%\%tracker_id%_*.*
	FileMove, %source_track%, %destination%\%tracker_id%_*.*
	
}