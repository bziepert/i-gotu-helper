return

check_for_atrip_pc_location() {
	
	check_for_ini_setting()
	
}

check_for_ini_setting() {
	
	IniRead, exe_dir_check, settings\settings.ini, atrippc, dir
	if (exe_dir_check="ERROR" or exe_dir_check="") {
		
		exe_dir = C:\Program Files (x86)\Mobile Action\atrip\GpsPlatformExe.exe
		
		ifExist, C:\Program Files (x86)\Mobile Action\atrip\GpsPlatformExe.exe
			IniWrite, %exe_dir%, settings\settings.ini, atrippc, dir
		IfNotExist, C:\Program Files (x86)\Mobile Action\atrip\GpsPlatformExe.exe
			show_add_application_dialog()
		RETURN	
		
	} else {
	
		IfNotExist, %exe_dir_check%
			message := ""
			IniWrite,%message%, settings\settings.ini, atrippc, dir
			check_for_ini_setting()
		RETURN
	
	}
	
}

show_add_application_dialog() {

	Gui, Add, Text,, @trip PC could not be found. Please download and install @trip PC from:
	Gui, Font, cBlue underline
	Gui, Add, Text, ym gGotoSite, www.i-gotu.com
	Gui, Font, cBlack norm
	
	Gui, Font, cBlue bold
	Gui, Add, Text, Please install the program in English. For this you must select the advanced installation options.
	Gui, Font, cBlack norm
	
	Gui, Add, Text, xm, `nProceed when you have installed to software.
	
	Gui, Add, Button, ginstalled, I have installed @trip PC
	Gui, Add, Button, gGuiClose, Cancel

	Gui, Show,, @trip PC could not be found
	
	WinWaitClose,, @trip PC could not be found.
	
}

GotoSite:
	Run, %A_GuiControl%
Return

installed:
	gui, destroy
	retry_to_auto_add_dir()
RETURN

retry_to_auto_add_dir() {
	
	exe_dir = C:\Program Files (x86)\Mobile Action\atrip\GpsPlatformExe.exe
		
	ifExist, C:\Program Files (x86)\Mobile Action\atrip\GpsPlatformExe.exe
		IniWrite, %exe_dir%, settings\settings.ini, atrippc, dir
	IfNotExist, C:\Program Files (x86)\Mobile Action\atrip\GpsPlatformExe.exe
		add_application_dir()
	RETURN
	
}

add_application_dir() {
	
	message=
	(
		Please select GpsPlatformExe.exe.
		
		By default the path should be "C:\Program Files (x86)\Mobile Action\atrip\GpsPlatformExe.exe".
	)
	StringReplace , message, message, %A_Tab%,,All
	
	Gui, Add, Text,, %message%
	Gui, Add, Button, gselect, Select GpsPlatformExe.exe
	Gui, Add, Button, gGuiClose, Cancel
	Gui, Show,, Please select GpsPlatformExe.exe
	
	WinWaitClose,, Please select GpsPlatformExe.exe
	
}

select:
	FileSelectFile, exe_dir, 1, C:\Program Files (x86)\Mobile Action\atrip\GpsPlatformExe.exe
	
	if ErrorLevel
	{
		restart_program()
	}
	
	Gui, Destroy
	IniWrite, %exe_dir%, settings\settings.ini, atrippc, dir
RETURN