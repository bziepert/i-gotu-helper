﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

IfExist, include.ahk
{
	FileDelete, include.ahk
}

Loop, functions\*.ahk, 0, 1
{
	text = %text%#Include %A_LoopFileLongPath%`n
}

FileAppend, %text%, include.ahk