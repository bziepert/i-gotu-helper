﻿; **********************************************************************
; ***** Script settings
; **********************************************************************
#NoEnv
SendMode Input
SetWorkingDir %A_ScriptDir%
DetectHiddenWindows On
#SingleInstance Force

; **********************************************************************
; ***** Startup main function
; **********************************************************************

global macro_paused
macro_paused = 0

restart_script_as_admin()
run_macro()

RETURN

; **********************************************************************
; ***** Hotkeys
; **********************************************************************

; ***** close the program *****
;LControl & ESC::
;
;	WinClose %A_ScriptDir%\macro.ahk
;	WinClose %A_ScriptDir%\macro.exe
;	ExitApp
;
;RETURN

; ***** restart the macro *****
;LControl & r::
;
;	WinClose %A_ScriptDir%\macro.ahk
;	WinClose %A_ScriptDir%\macro.exe
;	run_macro()
;
;RETURN

; ***** pause the macro *****
LControl & p::
;	toggle_pause_macro()
	PostMessage, 0x111, 65306,,, %A_ScriptDir%\macro.ahk  ; pause macro
	PostMessage, 0x111, 65306,,, %A_ScriptDir%\macro.exe  ; pause macro
RETURN

; **********************************************************************
; ***** Functies
; **********************************************************************

; ***** pause macro *****
toggle_pause_macro() {

	; disable block input
	if(macro_paused) {
		SetTimer, block_input, On
		macro_paused = 0
	} else {
		blockinput, Off
		SetTimer, block_input, Off
		macro_paused = 1
	}
	
	; toggle pause
	PostMessage, 0x111, 65306,,, %A_ScriptDir%\macro.ahk  ; pause macro
	PostMessage, 0x111, 65306,,, %A_ScriptDir%\macro.exe  ; pause macro

}

; ***** run helper as admin *****
restart_script_as_admin() {

	if not A_IsAdmin
	{
		Run *RunAs "%A_ScriptFullPath%"
	}
	
}

; ***** run macro *****
run_macro() {

	StringRight, file_extension, A_ScriptName, 3
	if(file_extension = "ahk") {
		run, ahk_source\includer.exe,,IncluderPID
		WinWaitClose, ahk_pid %IncluderPID%
	}

	IfExist, macro.exe
	{
		Run macro.exe
	} else {
		Run macro.ahk
	}
	
	; SetTimer, block_input
	
	
}

; **********************************************************************
; ***** Threads
; **********************************************************************

; ***** block the user if needed input *****
block_input:

	IniRead, blockinput, settings\settings.ini, macro, blockinput, 0
	if(blockinput){
		blockinput, On
	} else {
		blockinput, Off
	}

RETURN