#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.
#SingleInstance force
SetTitleMatchMode,2
DetectHiddenWindows,on

global block_input_active
block_input_active := 0

restart_program() {
	
	; ***** save gui settings *****
	IfWinExist, i-gotU helper, Run helper
	{
		WinGetPos,gui_x,gui_y,,, i-gotU helper, Run helper
		IniWrite, x%gui_x% y%gui_y%, settings\settings.ini, general, gui_position
		gui, submit
		write_settings_to_ini()
	}
	
	; ***** restart helper *****
	; * this will also restart the macro *
	StringRight, file_ending, A_ScriptFullPath, 3
	if (file_ending = "ahk") {
		Run *RunAs "%A_ScriptDir%\igotUhelper.ahk"
		WinClose, %A_ScriptDir%\igotUhelper.ahk
	} else {
		Run *RunAs "%A_ScriptDir%\igotUhelper.exe"
		SetTitleMatchMode,2
DetectHiddenWindows,on
		WinClose, %A_ScriptDir%\igotUhelper.exe
	}
	
	ExitApp
	
}

close_program() {
	
	; ***** save gui settings *****
	IfWinExist, i-gotU helper, Run helper
	{
		WinGetPos,gui_x,gui_y,,, i-gotU helper, Run helper
		IniWrite, x%gui_x% y%gui_y%, settings\settings.ini, general, gui_position
		gui, submit
		write_settings_to_ini()
	}
	
	; ***** close helper and macro *****
	WinClose, %A_ScriptDir%\igotUhelper.ahk
	WinClose, %A_ScriptDir%\igotUhelper.exe
	ExitApp
	
}

run_as_admin()

main()
RETURN

#include ahk_source\include.ahk

main() {

	FileCreateDir, files
	FileCreateDir, files/track
	FileCreateDir, files/gpx
	FileCreateDir, files/csv
	FileCreateDir, settings
	IniWrite, 0, settings\settings.ini, macro, blockinput
	
	; ***** make sure people read the notifications *****
	trick_question()
	
	; ***** make sure that @trip PC is installed and the location is saved *****
	check_for_atrip_pc_location()
	
	; ***** show the gui ******
	create_gui()
	
}

trick_question() {
	
	IniRead, trick_question, settings\settings.ini, general, trick_question, 0
	
	if(trick_question) {
		return
	}
	
	Loop {
	Msgbox, 49, Please read all messages, It is very important that you read all messages carefully. If you understand this please confirm with cancel. Otherwise you will see this message again.
		IfMsgBox Cancel
		{
			IniWrite, 1, settings\settings.ini, general, trick_question
			break
		}
	}

}

run_as_admin() {
	if not A_IsAdmin
	{
		restart_program()
	}
}